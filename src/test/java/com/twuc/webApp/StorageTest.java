package com.twuc.webApp;

import com.twuc.webApp.domain.Bag;
import com.twuc.webApp.domain.Storage;
import com.twuc.webApp.domain.Ticket;
import com.twuc.webApp.exception.FullException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {

    private static Storage createStorage() {
        return new Storage(2, 0, 0);
    }

    @Test
    void shouldReturnATicketWhenGiveBag() throws FullException {
        Storage storage = createStorage();
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        assertNotNull(ticket);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void shouldReturnATicketWhenSaveNoting() throws FullException {
        Storage storage = createStorage();
        Bag noting = null;

        Ticket ticket = storage.save(noting);
        assertNotNull(ticket);
    }

    @Test
    void shouldReturnASaveBagWhenGiveATicket() throws Exception {
        Storage storage = createStorage();
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        Bag otherBag = new Bag();
        storage.save(otherBag);

        Bag retrievedBag = storage.retrieve(ticket);
        assertSame(savedBag, retrievedBag);
        assertNotSame(otherBag, retrievedBag);
    }

    @Test
    void shouldReturnAnErrorMessageWhenAnInvalidTicket() throws Exception {
        Storage storage = createStorage();
        Ticket ticket = new Ticket();

        assertThrows(IllegalAccessException.class,
                () -> storage.retrieve(ticket),
                "Invalid Ticket");
    }

    @Test
    void shouldReturnAnErrorMessageWhenGiveEmptyStorage() throws Exception {
        Storage storage = createStorage();
        Ticket ticket = new Ticket();

        assertThrows(IllegalAccessException.class,
                () -> storage.retrieve(ticket),
                "Invalid Ticket");
    }

    @Test
    void shouldReturnAnErrorMessageWhenGiveTicketButIsUse() throws Exception {
        Storage storage = createStorage();
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        storage.retrieve(ticket);

        assertThrows(IllegalAccessException.class,
                () -> storage.retrieve(ticket),
                "Invalid Ticket");
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void shouldReturnNothingWhenGiveValidAndSavingNoting() throws Exception {
        Storage storage = createStorage();
        Bag noting = null;
        Ticket ticket = storage.save(noting);

        Bag retrievedBag = storage.retrieve(ticket);
        assertNull(retrievedBag);
    }

    @Test
    void shouldReturnATicketWhenGiveAnEmptyStorageAndCapacityIs2() throws FullException {
        int capacity = 2;
        Storage storage = new Storage(capacity, 0, 0);

        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
    }

    @Test
    void shouldReturnErrorMessageWhenGiveAFullStorageAndCapacityIs2() throws FullException {
        int capacity = 2;
        Storage storage = new Storage(capacity, 0, 0);
        storage.save(new Bag());
        storage.save(new Bag());

        assertThrows(FullException.class,
                () -> storage.save(new Bag()),
                "Insufficient capacity");
    }

    @Test
    void shouldReturnErrorMessageWhenSaving2BagAndStorageCapacityIs1() throws FullException {
        int capacity = 1;
        Storage storage = new Storage(capacity, 0, 0);

        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
        assertThrows(FullException.class,
                () -> storage.save(new Bag()),
                "Insufficient capacity");
    }

    @Test
    void shouldReturnTicketWhenGaveAFullStorageAndRetrieveOneBagThenSaveBag() throws Exception {
        final int SMALL = 1;
        int capacity = 1;
        Storage storage = new Storage(capacity, 0, 0);
        Ticket ticket = storage.save(new Bag(SMALL), SMALL);

        storage.retrieve(ticket);
        Ticket savedTicket = storage.save(new Bag(SMALL), SMALL);
        assertNotNull(savedTicket);
    }

    @Test
    void shouldNotReturnTicketWhenBigBagToSmallCapacity() {
        int smallCapacity = 1;
        int mediumCapacity = 1;
        int bigCapacity = 1;
        final int BIG = 3;
        final int SMALL = 1;
        Storage storage = new Storage(smallCapacity, mediumCapacity, bigCapacity);



        assertThrows(IllegalAccessError.class,
                () -> storage.save(new Bag(BIG), SMALL),
                "Insufficient capacity");
    }
}