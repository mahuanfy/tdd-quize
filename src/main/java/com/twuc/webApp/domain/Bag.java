package com.twuc.webApp.domain;

public class Bag {
    private int type;

    public Bag(int type) {
        this.type = type;
    }

    public Bag() {
        this.type = 1;
    }

    public int getType() {
        return type;
    }
}
