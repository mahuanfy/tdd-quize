package com.twuc.webApp.domain;


import com.twuc.webApp.exception.FullException;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private Map<Ticket, Bag> bags = new HashMap<>();
    private final int smallCapacity;
    private final int mediumCapacity;
    private final int bigCapacity;
    private final String[] capacityName = {"small", "medium", "big"};
    private final static int BIG = 3;
    private final static int MEDIUM = 2;
    private final static int SMALL = 1;
    public Ticket save(Bag bag){
        return this.save(bag, SMALL);
    }
    public Ticket save(Bag bag, int saveType) throws FullException {
        int capacity = 0;
        if(saveType == SMALL){
            capacity = smallCapacity;
        }else if(saveType == MEDIUM){
            capacity = mediumCapacity;
        }else if(saveType == BIG){
            capacity = bigCapacity;
        }
        if (bags.size() >= capacity) {
            throw new FullException("Insufficient capacity");
        }
        if(bag.getType() > saveType){
            throw new IllegalAccessError(String.format("Cannot save your bag: %s %s",capacityName[bag.getType()-1],capacityName[saveType]) );
        }

        Ticket ticket = new Ticket();

        bags.put(ticket, bag);
        return ticket;
    }

    public Storage(int smallCapacity, int mediumCapacity, int bigCapacity) {
        this.smallCapacity = smallCapacity;
        this.mediumCapacity = mediumCapacity;
        this.bigCapacity = bigCapacity;
    }

    public Bag retrieve(Ticket ticket) throws Exception {
        if (!bags.containsKey(ticket)) {
            throw new IllegalAccessException("Invalid Ticket");
        }
        Bag bag = bags.get(ticket);
        bags.remove(ticket);
        return bag;
    }
}
