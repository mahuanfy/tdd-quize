package com.twuc.webApp.exception;

public class FullException extends RuntimeException {
    public FullException() {
        super();
    }

    public FullException(String message) {
        super(message);
    }
}
